import styled from 'styled-components'

export const Container = styled.div`
    background: #2b2e39;
    margin: 0 auto;
    width: 80%;
    max-width: 800px;
    padding: 14px;
    border-radius: 14px;
    margin-top: 14px;
`

export const PersonalData = styled.div`
    font-size: 20px;
    color: antiquewhite;
    width: 80%;
    float: left;
`

export const InputSection = styled.div`
    margin-bottom: 25px;
`

export const InputTheme = styled.input`
    background: #2b2e39;
    margin: auto;
    width: 100%;
    border-radius: 14px;
    margin-top: 10px;
    padding-top: 10px;
    padding-bottom: 10px;
    color: white;
`

export const AddButton = styled.button`
    border-radius: 10px;
    border: 2px solid #034d19;
    cursor: pointer;
    color: antiquewhite;
    padding: 6px 24px;
    background-color: #309c26;
    width: 100%;

    &:hover {
        background-color:  antiquewhite;
        color: #309c26;
    }
`
export const ItemList = styled.div`
    border-radius: 15px;
    padding: 15px;
    margin-bottom: 10px;
    display: flex;
    border: 2px solid antiquewhite;
`

export const Buttons = styled.div`
    width: 100px;
`
export const RemoveButton = styled.button`
    border-radius: 10px;
    border: 2px solid #942911;
    cursor: pointer;
    color: antiquewhite;
    padding: 6px 24px;
    background-color: #d0451b;
    width: 100%;

    &:hover {
        background-color:  antiquewhite;
        color: #d0451b;
    }
`

export const EditButton = styled.button`
    border-radius: 10px;
    border: 2px solid #1f065a;
    cursor: pointer;
    color: antiquewhite;
    padding: 6px 24px;
    background-color: #4811d3;
    width: 100%;

    &:hover {
        background-color:  antiquewhite;
        color: #4811d3;
    }
`

export const ErrorMessage = styled.p`
    margin-top: 5px;
`

export const Loading = styled.h1`
    color: blue;
    text-align: center;
`