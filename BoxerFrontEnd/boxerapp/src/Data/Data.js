const url = "http://localhost:8080/";

export const getUrl = () => {
    return url;
}

let studentsTmp = [{name: "aaa", surname: "bbb", id: 1},
                    {name: "ccc", surname: "ddd", id: 2},
                    {name: "eee", surname: "fff", id: 3}];

export const getAllStudents = async () => {
    /*let a = [];
    fetch(url + "getAllStudents").then(
                response => response.json()
            ).then(
                json => resolve(json)
            )*/
    let result = []; 
    fetch(url + "getAllStudents").then(
        response => response.json()
    ).then(
        json => json.map(item => result.push(item))
    )
    return result;
}

export const removeStudent = (studentToRemove) => {
    let indexToRemove = studentsTmp.indexOf(studentToRemove);
    if( indexToRemove > -1) {
        studentsTmp.splice(indexToRemove,1);
    }
}

export const addStudent = (name, surname) => {
    let newStudent = {name: name, surname: surname, id: 5};
    studentsTmp.push(newStudent);
}

export const getStudentById = (id) => {
    for(let i=0; i< studentsTmp.length; i++) {
        if (id == studentsTmp[i].id) {
            return studentsTmp[i];
        }
    }
    return [];
}

export const updateStudent = (newStudent) => {
    for(let i=0; i< studentsTmp.length; i++) {
        if (newStudent.id == studentsTmp[i].id) {
            studentsTmp[i] = newStudent;
        }
    }
}