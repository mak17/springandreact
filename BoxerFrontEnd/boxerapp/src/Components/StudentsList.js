import React from 'react';
import * as DataHandler from '../Data/Data.js';
import StudentComponent from './Student.js';
import { AddButton, Loading } from '../Theme/Theme.js'

class StudentsList extends React.Component {

    state = {
        students: [],
        isLoading: true
    }

    componentDidMount = async () => {
        this.setState({ isLoading: true });

        fetch("http://localhost:8080/getAllStudents").then(
            response => response.json()).then(
                json => this.setState({
                    students: json,
                    isLoading: false
                })
            )
    }

    updateList = () => {
        this.setState({ isLoading: true });

        fetch("http://localhost:8080/getAllStudents").then(
            response => response.json()).then(
                json => this.setState({
                    students: json,
                    isLoading: false
                })
            )
    }

    redirectToMainPage = () => {
        this.props.history.push("/");
    }

    goToAddNewStudentForm = () => {
        this.props.history.push("/newStudent");
    }
    
    render() {
        const { students, isLoading } = this.state;

        if(isLoading) {
            return (<Loading>Loading...</Loading>)
        }

        return (
            <div>
                {students.map(student => <StudentComponent student={student} 
                                            key={student.id} 
                                            updateList={this.updateList}/>)}
                <AddButton onClick={this.goToAddNewStudentForm}>Add</AddButton>
            </div>
        )
    }
}

export default StudentsList