import React from 'react';
import { resolve, reject } from "q";
import { InputSection, InputTheme, AddButton, ErrorMessage, Loading } from '../Theme/Theme';

class EditStudent extends React.Component {

    state = {
        student: "",
        validName: true,
        validSurname: true,
        isLoading: true,
        isExist: true,
    }

    componentDidMount = async () => {

        this.setState({ isLoading: true });

        fetch("http://localhost:8080/getStudentById/" + this.props.match.params.studentId).then(
            response => response.json()).then(
                json => this.setState({
                    student: json,
                    isExist: this.props.match.params.studentId == json.id,
                    validName: true,
                    validSurname: true,
                    isLoading: false
                })
            )
    }

    saveStudent = () => {
        this.setState({validName: this.state.student.name.length > 0})
        this.setState({validSurname: this.state.student.surname.length > 0})
        if ( this.state.validName && this.state.validSurname) {
            
            fetch("http://localhost:8080/updateStudent/" + this.state.student.id + "/" + this.state.student.name + "/" + this.state.student.surname).then(
                response => {
                    if(response.ok) {
                        resolve(response);
                        this.redirectToMainPage();
                    } else {
                        reject(response);
                    }
                }
            )
        }
    }

    updateName = event => {
        let student = this.state.student;
        student.name = event.target.value;
        this.setState({student: student})
    }

    updateSurname = event => {
        let student = this.state.student;
        student.surname = event.target.value;
        this.setState({student: student})
    }

    redirectToMainPage = () => {
        this.props.history.push("/");
    }

    render() {

        if(this.state.isLoading) {
            return (<Loading>Loading...</Loading>)
        }
        
        if(!this.state.isExist) {
            return (<p>{this.props.history.push("/notFoundStudent")}</p>)
        }

        return (
            <div>
                <InputSection className={!this.state.validName ? "HasError" : ""}>
                    <div>Name:</div>
                    <div><InputTheme onChange={this.updateName} value={this.state.student.name}/></div>
                    {!this.state.validName ? <ErrorMessage>Name is required</ErrorMessage> : ''}
                </InputSection>
                <InputSection className={!this.state.validSurname ? "HasError" : ""}>
                    <div>Surname:</div>
                    <div><InputTheme onChange={this.updateSurname} value={this.state.student.surname}/></div>
                    {!this.state.validName ? <ErrorMessage>Surname is required</ErrorMessage> : '' }
                </InputSection>
                <AddButton onClick={this.saveStudent}>Save Changes</AddButton>
            </div>
        )
    }
}

export default EditStudent