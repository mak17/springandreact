import React from 'react';
import * as DataHandler from '../Data/Data.js';
import { resolve, reject } from "q";
import '../App.css'
import { ItemList, PersonalData, Buttons, RemoveButton, EditButton } from '../Theme/Theme';

class Student extends React.Component {

    state = {
        student: this.props.student
    }

    removeStudent = () => {
        fetch("http://localhost:8080/removeStudent/" + this.state.student.id).then(
            response => {
                if(response.ok) {
                    resolve(response);
                    this.props.updateList();
                } else {
                    reject(response);
                }
            }
        )
    }

    render() {
        const { student } = this.state;
        return (
            <ItemList>
               <PersonalData>{student.name + " " + student.surname}</PersonalData>
               <Buttons>
                    <RemoveButton onClick={this.removeStudent}>Remove</RemoveButton>
                    <a href={'/editStudent/' + student.id}><EditButton>Edit</EditButton></a>
               </Buttons>
               
            </ItemList>
        )
    }
}

export default Student