package com.boxer.app.Repositories;

import com.boxer.app.Models.Student;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Mak on 2019-10-01.
 */
public interface StudentRepository extends JpaRepository<Student, Long> {
}
